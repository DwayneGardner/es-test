// Grabbing our form and country selector so we can monitor and act on the submit and change events respectively
const form = document.querySelector('#form');
if (form) {
  form.addEventListener("submit", () => validateForm(event));
}
const country = form.querySelector('select[name="Country"]');
const postalCode = form.querySelector('input[name="PostalCode"]');
if(country && postalCode) {
  country.addEventListener("change", () => {changeCountry(country, postalCode)})
}

// Placing into variables so I can use on the form and within code
const emailRegExp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
const uk_zipcode = /([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z]))))\s?[0-9][A-Za-z]{2})/
const us_zipcode = /\d{5}(?:[-\s]\d{4})?/


function validateForm(event) {
  // Grabbing the container lets me target any child easily
  const passwd = form.querySelector('#passwd')
  const passwdconf = form.querySelector('#passwdconf')
  const email = document.querySelector('#form-email')
  const zipCode = document.querySelector('#form-postal')

  // Seperated each validation with testing in mind
  let validEmail = validateEmail(email);
  let validPassword = validatePassword(passwd, passwdconf)
  let validZipCode = validateZipCode(zipCode)

  if (!validEmail || !validPassword || !validZipCode) {
    event.stopPropagation();
    event.preventDefault();
  }
}

// I know a default validation was requested but it made sense to just disable the 
// zipcode input while a country wasn't selected. Should add validation for country on submit
const changeCountry = (country, postalCode) => {
  if (country.value === '') {
    postalCode.disabled = true;
  }
  else if (country.value === 'United Kingdom') {
    postalCode.disabled = false;
    // Have to get the source of the regex to allow it to be read as text
    // Could have also used toString()
    postalCode.pattern = uk_zipcode.source
  } else if (country.value === 'United States of America') {
    postalCode.disabled = false;
    postalCode.pattern = us_zipcode.source
  }
}

function validateZipCode (zipCode) {
  let zipInput = zipCode.querySelector('input')
  let zipRegExp = new RegExp(zipInput.pattern)
  if (zipRegExp.toString() === uk_zipcode.toString() || zipRegExp.toString() === us_zipcode.toString()) {
    let test = zipInput.value.length === 0 || zipRegExp.test(zipInput.value);
    // The below check is duplicated over the three validations, should seperate into own function
    if (!test) {
      zipCode.classList.add('error')
      return false
    } else {
      zipCode.classList.remove('error')
      return true;
    }
  } else {
    // Means the pattern was changed in the HTML (could be accidental, could be malicious)
    // so going to quietly fail for this test
    return false;
  }
}

function validatePassword (password, confirmPassword) {
  let first_password = password.querySelector('input')
  let second_password = confirmPassword.querySelector('input')

  // This will run first ignoring the rest of the script if wrong
  // If the password is invalid doesn't matter if the confirm matches
  // since it will need to be rewritten
  if (first_password.value.length < 4) {
    password.classList.add('error')
    return false;
  } else {
    password.classList.remove('error')
    return true;
  }
  
  if (first_password.value !== second_password.value) {
    confirmPassword.classList.add('error')
    return false;
  } else {
    confirmPassword.classList.remove('error')
    return true;
  }
}

function validateEmail (email) {
  emailInput = email.querySelector('input');
  let test = emailInput.value.length === 0 || emailRegExp.test(emailInput.value);
  if (!test) {
    email.classList.add('error')
    return false;
  } else {
    email.classList.remove('error')
    return true;
  }
}
