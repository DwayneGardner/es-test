const express = require('express');
// Request was the most recommended package for doing HTTP requests
const request = require('request');
// Moment to format the date on the page
const moment = require('moment');
const app = express();

// Using EJS because its the most similar to regular HTML as opposed to PUG/Jade
app.set('view engine', 'ejs');
app.use(express.static('public'));

app.get('/', (req, res) => {
    request('https://www.metaweather.com/api/location/44418/', function(error, response, body) {
        if(error) {
            // Could just hard code error message into EJS file and return true instead.
            res.render('index', {weather: null, error: 'Couldn\'t download file, please try again'})
        } else {
            const weather = JSON.parse(body);
            if(weather.consolidated_weather === undefined) {
                res.render('index', {weather: null, error: 'Couldn\'t download file, please try again'})
            } else {
                res.render('index', {weather: weather.consolidated_weather, error: null, moment: moment});
            }
        }
    })
});

app.listen(3000, () => {
    console.log('App listening on port 3000!');
});